<?php

class Building{
	// private access modifier disables directs access to an object's property or methods
	// protected access modifier allows inheritance of properties and methods to child classes. However, it will still disable direct access to its properties and methods.

	// private $name;
	// private $floors;
	// private $address;

	protected $name;
	protected $floors;
	protected $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}
}
class Condominium extends Building{
	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}

	public function setFloors($floors){
		$this->floors = $floors;
	}
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon city, Philippines');
$condominium = new Condominium ('Enzo Condo', 5, 'Buendia Avenue', 'Makati city');